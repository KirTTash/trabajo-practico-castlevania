package juego;

import java.awt.Color;
import java.awt.Image;

import entorno.Entorno;
import entorno.Herramientas;

public class Comodore {
	private double PosX;
	private double PosY;
	private double Alto;
	private double Largo;
	private Image imagenComodore;
	
	public Comodore (double PosX, double PosY) {
		this.PosX = PosX;
		this.PosY = PosY;
		this.Largo = 40;
		this.Alto = 40;
		this.imagenComodore = Herramientas.cargarImagen("comodore.png");
	}
	
	public void dibujarComodore(Entorno entorno) {
		entorno.dibujarImagen(imagenComodore, PosX, PosY, 0.06, 0.3);
		//entorno.dibujarRectangulo(PosX, PosY, Largo, Alto, 0, Color.green);
	}
	
	public boolean colisionConPersonaje(Personaje personaje) {  
		return (this.PosX - this.Largo/2 < personaje.getX() + personaje.getAncho()/2 && 
				this.PosX + this.Largo/2 > personaje.getX()- personaje.getAncho()/2  && 
				this.PosY - this.Alto/2 < personaje.getY() + personaje.getAlto()/2 && 
				this.PosY + this.Alto/2 > personaje.getY() - personaje.getAlto()/2); 
		}

}
