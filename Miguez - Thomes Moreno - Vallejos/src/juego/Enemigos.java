package juego;

import java.awt.Color;
import java.awt.Image;
import java.util.Random;

import entorno.Entorno;
import entorno.Herramientas;

public class Enemigos {
	private double PosY;
	private double PosX;
	private double Alto;
	private double Largo;
	private int Direccion;
	private int TiempoDeDisparo;
	private Image imagenVelociraptor;
	
	public Enemigos(double posX, double posY, double alto, double largo, int Tiempo) {
		PosY = posY;
		PosX = posX;
		Alto = alto;
		Largo = largo;
		Direccion = 1;
		TiempoDeDisparo = Tiempo;
		this.imagenVelociraptor = Herramientas.cargarImagen("velociraptor.png");
	}
		
	public void dibujarEnemigo(Entorno entorno) { 
			entorno.dibujarImagen(imagenVelociraptor, PosX, PosY, 0.1, 0.2);
			//entorno.dibujarRectangulo(this.PosX, this.PosY, this.Largo, this.Alto, 0, Color.cyan);
		}
	
	public boolean colisionConPiso(Piso Piso){  
			return (this.PosX - this.Largo/2 < Piso.getPosX() + Piso.getLargo()/2 && 
					this.PosX + this.Largo/2 > Piso.getPosX()- Piso.getLargo()/2  && 
					this.PosY - this.Alto/2 < Piso.getPosY() + Piso.getAlto()/2 && 
					this.PosY + this.Alto/2 > Piso.getPosY() - Piso.getAlto()/2); 

	}
	
	public boolean colisionConPisos(Piso[] pisos) {
		for (int i = 0 ; i < pisos.length ; i++ ) {
			if (this.colisionConPiso(pisos[i])) {
				return true;
			}
		}
		return false;
	}
	
	public boolean colisionIzquierda(Entorno entorno) {  // Colision detectada con El borde izquierdo del entorno
		return this.PosX - this.Largo/2 <= 0; 
		
	}

	public boolean colisionDerecha(Entorno entorno) {  // Colision detectada con el borde derecho del entorno
		return this.PosX + this.Largo/2 >= entorno.ancho();
	}
	
	
	public void cambiarDireccion() {   /// Se Multiplica la Direccion * -1 Para que su signo cambie, Al ser X+ Se mueve a la Derecha
		this.Direccion *= -1;          /// Al ser X- Se mueve a la izquierda. Se Usa junto con Movimiento	
	}                                  // Para que se mueva una velocidad Fija de 5 (Al ser * -1, el total seria 5- o 5+)
	
	public void mover() {
		this.PosX += 3 * Direccion;
	}
	private static int getRandomNumberInRange(int min, int max) {
		Random r = new Random();
		return r.nextInt((max - min) + 1) + min;
	}
	
	public static void crearEnemigo(Enemigos[] enemigos) {
		for (int i = 0 ; i < enemigos.length ; i++) {
			if (enemigos[i] == null) {
				enemigos[i] = new Enemigos(700,100,60,60,getRandomNumberInRange(20,80));
				return;
			}
		}
	}
	public boolean colisionConPersonaje(Personaje personaje){  
		return (this.PosX - this.Largo/2 < personaje.getX() + personaje.getAncho()/2 && 
				this.PosX + this.Largo/2 > personaje.getX()- personaje.getAncho()/2  && 
				this.PosY - this.Alto/2 < personaje.getY() + personaje.getAlto()/2 && 
				this.PosY + this.Alto/2 > personaje.getY() - personaje.getAlto()/2); 
		}
	
	public void caer() {
		PosY += 3;		
	}
	
      ///////////////////////////////////////////////////////////SETTERS Y GETTERS////////////////////////////////


	public double getPosY() {
		return PosY;
	}

	public void setPosY(int posY) {
		PosY = posY;
	}

	public double getPosX() {
		return PosX;
	}

	public void setPosX(int posX) {
		PosX = posX;
	}

	public double getAlto() {
		return Alto;
	}

	public void setAlto(int alto) {
		Alto = alto;
	}

	public double getLargo() {
		return Largo;
	}

	public void setLargo(int largo) {
		Largo = largo;
	}

	public int getDireccion() {
		return Direccion;
	}

	public void setDireccion(int direccion) {
		Direccion = direccion;
	}

	public int getTiempoDeDisparo() {
		return TiempoDeDisparo;
	}

	public void setTiempoDeDisparo(int tiempoDeDisparo) {
		TiempoDeDisparo = tiempoDeDisparo;
	}

	public Image getImagenVelociraptor() {
		return imagenVelociraptor;
	}

	public void setImagenVelociraptor(Image imagenVelociraptor) {
		this.imagenVelociraptor = imagenVelociraptor;
	}



	

	
		
}

