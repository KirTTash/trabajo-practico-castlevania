package juego;
import entorno.*;
import java.awt.Color;

public class Interfaz {
	private String Vidas;
	private String Puntos;
	private String Kills;
	private int ContadorVidas;
	private int ContadorPuntos;
	private int ContadorKills;
	
	
	
	public Interfaz(int ContadorVidas,int ContadorPuntos,int ContadorKills) { // Constructor Interfaz
		this.Vidas = "Lives:";
		this.Puntos = "Score:";
		this.Kills = "Kills:";
		this.ContadorVidas = ContadorVidas;
		this.ContadorPuntos = ContadorPuntos;
		this.ContadorKills = ContadorKills;
		
	}
	
	public void dibujarInterfaz(Entorno entorno) {  // Dibujador de interfaz
		entorno.cambiarFont(Vidas,18, Color.white);
		entorno.escribirTexto(Vidas, 10, 17);
		String ContadorVidasString = Integer.toString(ContadorVidas);
		entorno.escribirTexto(ContadorVidasString, 60, 17);
		entorno.escribirTexto(Puntos, 380, 17);
		String ContadorPuntosString = Integer.toString(ContadorPuntos);
		entorno.escribirTexto(ContadorPuntosString, 450, 17);
		entorno.cambiarFont(Puntos,18, Color.white);
		entorno.escribirTexto(Kills, 750, 17);
		String ContadorKillsString = Integer.toString(ContadorKills);
		entorno.escribirTexto(ContadorKillsString, 800, 17);
		entorno.cambiarFont(Kills,18, Color.white);
		
	}
	
	 public void restarVidas() {
	    this.ContadorVidas -= 1;
	 }
	
	public void sumarPuntos() {
        this.ContadorPuntos += 50;
    }

    public void sumarKills() {
        this.ContadorKills += 1;
    }
	
	/////////////////////////////// Getters and Setters ////////////////////////////////

	public String getVidas() {
		return Vidas;
	}

	public void setVidas(String vidas) {
		Vidas = vidas;
	}

	public String getPuntos() {
		return Puntos;
	}

	public void setPuntos(String puntos) {
		Puntos = puntos;
	}

	public String getKills() {
		return Kills;
	}

	public void setKills(String kills) {
		Kills = kills;
	}

	public int getContadorVidas() {
		return ContadorVidas;
	}

	public void setContadorVidas(int contadorVidas) {
		ContadorVidas = contadorVidas;
	}

	public int getContadorPuntos() {
		return ContadorPuntos;
	}

	public void setContadorPuntos(int contadorPuntos) {
		ContadorPuntos = contadorPuntos;
	}

	public int getContadorKills() {
		return ContadorKills;
	}

	public void setContadorKills(int contadorKills) {
		ContadorKills = contadorKills;
	}
	

	

}
