package juego;

import java.awt.Color;
import java.awt.Image;

import entorno.Entorno;
import entorno.Herramientas;
import entorno.InterfaceJuego;

public class Juego extends InterfaceJuego {
	// El objeto Entorno que controla el tiempo y otros
	private Entorno entorno;
	private Personaje personaje;
	private Interfaz interfaz;
	private Piso pisos[];
	private Enemigo enemigos[];
	private Laser laser[];
	private Laser rayo; //
	private int tiempo;
	private Image fondo;
	private Pantalla pantalla;
	private Pantalla pantallaGanaste;
	private Comodore comodore;
	boolean ganaste = false;

	// Variables y métodos propios de cada grupo
	// ...

	Juego() {
		// Inicializa el objeto entorno
		this.entorno = new Entorno(this, " Castlevania Barbarianna Viking Edition", 900, 680);
		this.personaje = new Personaje(20, 600, Color.green);
		this.tiempo = 0;
		/////////////////////////////// Laser de enemigos y personaje ///////////////////////////
		this.laser = new Laser[6];
		/////////////////////////////// Pantallas y comodre ///////////////////////////
		this.comodore = new Comodore(800, 45);
		this.pantalla = new Pantalla(entorno.ancho()/2 - 200,entorno.alto()/2,"Perdiste");
		this.pantallaGanaste = new Pantalla(entorno.ancho()/2 - 200,entorno.alto()/2,"Ganaste");
		/////////////////////////////// Enemigos ///////////////////////////
		this.enemigos = new Enemigo[6];
		this.enemigos[0] = new Enemigo (700,100,60,60,80);
		this.enemigos[1] = new Enemigo (600,100,60,60,80); 															
		/////////////////////////////// Pisos /////////////////////////////
		this.pisos = new Piso[5];
		this.pisos[0] = new Piso(450, 672, 15, 900); 
		this.pisos[1] = new Piso(337.5, 536, 15, 750); 
		this.pisos[2] = new Piso(550, 400, 15, 750); 
		this.pisos[3] = new Piso(337.5, 264, 15, 750);
		this.pisos[4] = new Piso(550, 128, 15, 750);
		//////////////////////////////// Interfaz ////////////////////
		this.interfaz = new Interfaz(1, 0, 0);
		this.fondo = Herramientas.cargarImagen("castillo tp.png");

		
		// Inicializar lo que haga falta para el juego
		// ...

		// Inicia el juego!
		this.entorno.iniciar();

	}

	/**
	 * Durante el juego, el método tick() será ejecutado en cada instante y por lo
	 * tanto es el método más importante de esta clase. Aquí se debe actualizar el
	 * estado interno del juego para simular el paso del tiempo (ver el enunciado
	 * del TP para mayor detalle).
	 */
	public void tick() {
		// Procesamiento de un instante de tiempo
		// ...
		
		if (personaje.getVidas() == 0) {
			pantalla.dibujarPantalla(entorno);
		}
		if (ganaste) {
		pantallaGanaste.dibujarPantalla(entorno);
		}

		if (personaje.getVidas() != 0 &&
				!ganaste) {		
			entorno.dibujarImagen(fondo, entorno.ancho()/2, entorno.alto()/2, 0);
			personaje.dibujar(entorno);
			comodore.dibujarComodore(entorno);
			interfaz.dibujarInterfaz(entorno);
			tiempo++;			
			
			//////////////////////////////////////// Colision de Comodore con personaje ////////////////////////
			if (comodore.colisionConPersonaje(personaje)) {
				ganaste = true;
			}

			///////////////////////////////////// Seccion Enemigos//////////////////////////
			//////////////////////////////////////////////// Laser de los Enemigos ////////////////
			for (int i = 0; i < laser.length ; i++) { // For N°1
				if (laser[i] != null) {	
					laser[i].dibujarLaser(entorno);	
					laser[i].mover();
					
					if (laser[i].colisionConPersonaje(personaje)) {
						personaje.restarVidas();
					}
					
					if (laser[i].colisionConPersonaje(personaje) && ! personaje.seAgacho && ! personaje.onGround) {
						personaje.restarVidas();
					}
					
					if (laser[i].colisionDerecha(entorno) ||laser[i].colisionIzquierda(entorno) || enemigos[i] == null){
						laser [i] = null;
					}
				}
			}			
			
			///////////////////////////////////// Colisiones y Laser de Enemigos/////////////////////////
			for (int i = 0; i < enemigos.length; i++) {
				if (enemigos[i] != null) {		
					if (tiempo == enemigos[i].getTiempoDeDisparo()) {
						Laser.crearLaser(laser,enemigos[i]);
					}
					if (enemigos[i].colisionConPersonaje(personaje)) {
						personaje.restarVidas();;
					}
					if (enemigos[i].colisionConPisos(pisos)) {
						enemigos[i].mover();
					}else {
							enemigos[i].caer();  /// Aumenta el Y del enemigo, para caer
					}				
					if (enemigos[i].colisionDerecha(entorno)) {  /// Si colisiona , Cambia de direccion
                        enemigos[i].cambiarDireccion();
                        enemigos[i].setImagenVelociraptor(Herramientas.cargarImagen("velociraptor_izquierda.png"));
                    }
                    if (enemigos[i].colisionIzquierda(entorno)) {
                        enemigos[i].cambiarDireccion();
                        enemigos[i].setImagenVelociraptor(Herramientas.cargarImagen("velociraptor.png"));
					}
					if (enemigos[i].colisionIzquierda(entorno) && enemigos[i].colisionConPiso(pisos[0])) {
						enemigos[i] = null;   //// Si colisiona con la izquierda del entorno, en el piso 0, el enemigo se vuelve null
						}
					}       
				}
			///////////////////////////// Laser del personaje /////////////////////
			if (rayo != null) {
				if (rayo.colisionDerecha(entorno)) {
					rayo = null;
				}
			if (rayo != null) {
				if (rayo.colisionIzquierda(entorno)) {
					rayo = null;
				}
			}
			
			for (int i = 0; i < enemigos.length; i++) {
				
//				}
				if (rayo != null && enemigos[i] != null) {				
					if (rayo.colisionConEnemigo(enemigos[i])) {
						rayo = null;
						enemigos[i] = null;
						interfaz.sumarPuntos();
	                    interfaz.sumarKills();
					}
				}
			}
			}
		
			
			if (rayo != null ){ 
				rayo.dibujarLaser(entorno);
				rayo.mover();
			}
			

	      /////////////////////////Se dibujan los Enemigos Y se Crean nuevos en Caso de Null///////////////////////////
			for (int i = 0; i < enemigos.length; i++) {
				if (enemigos[i] != null) {                // Si los Enemigos son Diferente de null, se dibujan
					enemigos[i].dibujarEnemigo(entorno);  // Los rayos de barbarianna, al colisionar con los enemigo
					                                      //  vuelven null al enemigo
				}
			}
			if (tiempo == 150) { // Se crea otro dinosaurio pasado este tiempo, en el lugar null del arreglo
				Enemigo.crearEnemigo(enemigos); // 
				tiempo = 0;                         // Resetea el tiempo a 0
			}

			/////////////////////////////////// Fin de Seccion Enemigos///////////////////////////

			/////////////////////////////////// Seccion Pisos ///////////////////////////////////
			
			// Dibuja los pisos
			for (int i = 0; i < pisos.length; i++) { 
				pisos[i].DibujarPiso(entorno);
			}
			
	        ///////////////////////////////////// Fin de Seccion Pisos ///////////////////////          

	        //////////////////////////////////////Secccion Personaje/////////////////////////////               
	       						
			//si la tecla pulsada coincide y no choca con la izquierda del entorno entonces se mueve a la izquierda
			if (entorno.estaPresionada(entorno.TECLA_IZQUIERDA) && !personaje.chocoALaIzquierda(entorno)) {
				//personaje.moverIzquierda();
                personaje.setImagenParado(Herramientas.cargarImagen("Warrior_Idle_1_izquierda.png"));
                personaje.setDireccion(-1);
                personaje.mover();
			}

            //si la tecla pulsada coincide y no choca con la derecha del entorno entonces se mueve a la derecha
            if (entorno.estaPresionada(entorno.TECLA_DERECHA) && !personaje.chocoALaDerecha(entorno)) {
                personaje.setDireccion(1);
                personaje.mover();
                personaje.setImagenParado(Herramientas.cargarImagen("Warrior_Idle_1.png"));

                }
             

					//si la tecla pulsada coincide entonces el personaje se "agacha"
					if (entorno.estaPresionada(entorno.TECLA_ABAJO)) {
						personaje.agachado();
					} else {
						personaje.parado();
					}

					//si la tecla pulsada coincide entonces el personaje "salta"
					if (entorno.sePresiono(entorno.TECLA_ARRIBA)) {
						personaje.salto();
					}

					//si la tecla pulsada coincide entonces el personaje hace un salto mas largo
					if (entorno.sePresiono('u')) {
						personaje.saltoEspecial();
					}
	
					
					if (entorno.sePresiono('t') && rayo == null){
						this.rayo = personaje.disparar();				
					}
					
					//actua como gravedad
					if (!personaje.isOnGround()) {
						personaje.setVelocidadY(personaje.getVelocidadY() + personaje.getGravity());
					}
					personaje.setY(personaje.getY() + personaje.getVelocidadY());

					//detecta si esta chocando con los pisos
					//en este caso detecta los pies(abajo) del personaje
					boolean chocaPies = false;
					for (int i = 0; i < pisos.length; i++) {
						if (personaje.coincideX(pisos[i]) && personaje.piesChocan(pisos[i])) {
							chocaPies = true;
						}
					}
					
					//en este caso detecta la cabeza(arriba) del personaje
					boolean chocaCabeza = false;
					for (int i = 0; i < pisos.length; i++) {
						if (personaje.coincideX(pisos[i]) && personaje.cabezaChoca(pisos[i])) {
							chocaCabeza = true;
						}
					}

					//resetear la velocidad si esta chocando con los pies
					if (chocaPies) {
						{
							personaje.setVelocidadY(0);
						}
						personaje.setOnGround(true);
					} else {
						personaje.setOnGround(false);
					}
					
					//resetea la velocidad si esta chocando con la cabeza y imposibilidad que pase a travez de pisos
					if (chocaCabeza) {
						personaje.setVelocidadY(0);
						personaje.setOnGround(false);
						personaje.setY(personaje.getY() + 5);
					}

			
		} //// <<< Llave de finalizacio del If

////////////////////////////////////////////// Fin Seccion Personajes //////////////////////////////////////////////		

}//// <<<<< Esta Llave es el cierre de tick(), escribir de esto para arriba

	@SuppressWarnings("unused")
	public static void main(String[] args) {
		Juego juego = new Juego();
	}
}
