package juego;

import java.awt.Color;

import entorno.Entorno;

public class Laser {
	private double PosX;
	private double PosY;
	private double Alto;
	private double Largo;
	private int Direccion;

	
	public Laser(double posX, double posY, int direccion) {
		PosX = posX;
		PosY = posY;		
		Alto = 5;
		Largo = 30;
		Direccion = direccion;
	}

	public void dibujarLaser(Entorno entorno) { // Dibujar laser en el mismo y , pero en diferente x
		entorno.dibujarRectangulo(PosX,PosY, Largo, Alto, 0, Color.red);  // Debe ser direccional al piso en el que se encuentra
	} // Tambien verificar si enemigo se encuentra en ese piso (Todo esto dentro de juego)
	
	public static void crearLaser(Laser[] laser,Enemigo enemigo) {
		for (int i = 0 ; i < laser.length ;i++ ) {
			if (laser[i] == null) {
				laser[i] = new Laser(enemigo.getPosX(),enemigo.getPosY(),enemigo.getDireccion()); //+5?
				return;
			}			
		}
	}
	public static void crearLaserIndividual(Laser laser,Personaje personaje) {
			if (laser == null) {
				laser = new Laser(personaje.getX(),personaje.getY(),1); //+5?
				return;
			}			
		}
	
	public static void crearRayo(Laser[] rayo,Personaje personaje) {
		for (int i = 0 ; i < rayo.length ;i++ ) {
			if (rayo[i] == null) {
				rayo[i] = new Laser(personaje.getX(),personaje.getY(),1);
				return;			
			}
		}
	}
	
	public boolean colisionIzquierda(Entorno entorno) {  // Colision detectada con El borde izquierdo del entorno
		return this.PosX - this.Largo/2 <= 0; 
		
	}

	public boolean colisionDerecha(Entorno entorno) {  // Colision detectada con el borde derecho del entorno
		return this.PosX + this.Largo/2 >= entorno.ancho();
	}
	
	public boolean colisionConEnemigo(Enemigo enemigo){  
		return (this.PosX - this.Largo/2 < enemigo.getPosX() + enemigo.getLargo()/2 && 
				this.PosX + this.Largo/2 > enemigo.getPosX()- enemigo.getLargo()/2  && 
				this.PosY - this.Alto/2 < enemigo.getPosY() + enemigo.getAlto()/2 && 
				this.PosY + this.Alto/2 > enemigo.getPosY() - enemigo.getAlto()/2); 
		}
	
	public boolean colisionConPersonaje(Personaje personaje) {
		return (this.PosX - this.Largo/2 < personaje.getX() + personaje.getAncho()/2 && 
				this.PosX + this.Largo/2 > personaje.getX()- personaje.getAncho()/2  && 
				this.PosY - this.Alto/2 < personaje.getY() + personaje.getAlto()/2 && 
				this.PosY + this.Alto/2 > personaje.getY() - personaje.getAlto()/2); 
		}
	
	public void dispararDerecha() {
		this.PosX += 8;
	}
	public void dispararIzquierda() {
		this.PosX += -8;
	}
	
	public void mover() {
		this.PosX += 8* Direccion;
	}
	
	
	////////////////////////////////////////////////// GETTERS AND SETTERS //////////////////////////////////////////////////

	public double getPosY() {
		return PosY;
	}

	public void setPosY(double posY) {
		PosY = posY;
	}

	public double getPosX() {
		return PosX;
	}

	public void setPosX(double posX) {
		PosX = posX;
	}

	public double getAlto() {
		return Alto;
	}

	public void setAlto(double alto) {
		Alto = alto;
	}

	public double getLargo() {
		return Largo;
	}

	public void setLargo(double largo) {
		Largo = largo;
	}

	public int getDireccion() {
		return Direccion;
	}

	public void setDireccion(int direccion) {
		Direccion = direccion;
	}


	
	
}
