package juego;

import java.awt.Color;

import entorno.Entorno;

public class Pantalla {
	private String cartelito;
	private double PosX;
	private double PosY;
	
	public Pantalla (double PosX, double PosY, String cartel) {
		this.PosX = PosX;
		this.PosY = PosY;
		this.cartelito = cartel;
	}
	
	public void dibujarPantalla(Entorno entorno) {
		entorno.cambiarFont(cartelito,100, Color.white);
		entorno.escribirTexto(cartelito, PosX, PosY);
	}

}

