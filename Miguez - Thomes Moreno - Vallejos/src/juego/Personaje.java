package juego;

import java.awt.*;

import entorno.Entorno;
import entorno.Herramientas;

public class Personaje {
	private double x;
	private double y;
	private double ancho;
	private double alto;
	private Color color;
	public double velocidadX;
	public double velocidadY;
	public double gravity;
	private Image imagenParado;
	boolean onGround = true;
	boolean seAgacho = false;
	private int vidas;
	private int direccion;
	
	//constructor personaje
	public Personaje(double x, double y, Color color) {
		this.x = x;
		this.y = y;
		this.ancho = 40;
		this.alto = 60;
		this.color = color;
		this.velocidadX = 0;
		this.velocidadY = 0;
		this.gravity = 0.5;
		this.imagenParado = Herramientas.cargarImagen("Warrior_Idle_1.png");
		this.vidas = 1;
		direccion = 1;
	}
	
	//metodos
	//dibuja un rectagulo
	public void dibujar(Entorno e) {
		e.dibujarImagen(imagenParado,x, y, 0.06, 1.5);
	}
	
	public void crearLaserDelPersonaje (Laser laser) {
		if (laser == null) {
			laser = new Laser(this.getX(),this.getY(),1); //+5?
			return;
		}
	}
	
	public void mover() {   
		this.x += 5* direccion; 
	}
	
	//para simular que el personaje se agacha
	//se crea un metodo que divide el rectangulo
	public void agachado() {
		if(! seAgacho) {
			this.y = this.y + 15;
		}
		this.alto=30;
		this.seAgacho = true;
	}
	
	//vuelve a su estado default
	public void parado() {
		if(seAgacho) {
			this.y = this.y - 15;
		}
		this.ancho = 40;
		this.alto = 60;
		this.seAgacho = false;
	}
	
	//este metodo da la ilusion de saltar moviendolo hacia arriba
	public void salto() {
		if(onGround){
			this.velocidadY = -8.0;
			this.onGround = false;
		}
	}
	
	//este metodo hace lo mismo que "salto" pero lo hace mas alto
	public void saltoEspecial() {
		if(onGround){
			this.velocidadY = -14.0;
			this.onGround = false;
		}
	}
	 public void restarVidas() {
	        this.vidas -= 1;
	    }
	

	//detecta si el rectangulo choco a la derecha 
	public boolean chocoALaDerecha(Entorno entorno) {
		return (this.x + this.ancho/2 > entorno.ancho());
	}
	
	//detecta si el rectangulo choco a la izquierda 
	public boolean chocoALaIzquierda(Entorno entorno) {
		return (this.x - this.ancho/2 < 0);
	}
	
	//detecta si el rectangulo choco abajo
	public boolean chocoAbajo(Entorno entorno) {
		return (this.y + this.alto > 665);
	}

	//detecta si el rectangulo choco arriba
	public boolean chocoArriba(Entorno entorno) {
		return (this.y + this.alto/2 < 0);
	}
	
	//chequea si la X del personaje coincide con la X del piso
	public boolean coincideX(Piso pisoP) {
		return (this.x - this.ancho/2 < pisoP.getPosX() + pisoP.getLargo()/2 &&
				this.x + this.ancho/2 > pisoP.getPosX() - pisoP.getLargo()/2);
	}
	
	//detecta si colisiona  la pies del personaje coincide con la Y del piso
	public boolean piesChocan(Piso pisoP) {
		return	(this.y + this.alto/2 < pisoP.getPosY() + pisoP.getAlto()/2 && 
				 this.y + this.alto/2 > pisoP.getPosY() - pisoP.getAlto()/2);
	}
	
	//detecta si colisiona  la cabeza del personaje coincide con la Y del piso 
	public boolean cabezaChoca(Piso pisoP) {
		return	(this.y - this.alto/2 < pisoP.getPosY() + pisoP.getAlto()/2 && 
				 this.y - this.alto/2 > pisoP.getPosY() - pisoP.getAlto()/2);
	}

	/////////////////////desde aca inician los getters y setters//////////////////////////////////////////////////
	public double getX() {
		return x;
	}

	public void setX(double x) {
		this.x = x;
	}

	public double getY() {
		return y;
	}

	public void setY(double y) {
		this.y = y;
	}

	public double getAncho() {
		return ancho;
	}

	public void setAncho(double ancho) {
		this.ancho = ancho;
	}

	public double getAlto() {
		return alto;
	}

	public void setAlto(double alto) {
		this.alto = alto;
	}

	public double getVelocidadX() {
		return velocidadX;
	}

	public void setVelocidadX(double velocidadX) {
		this.velocidadX = velocidadX;
	}

	public double getVelocidadY() {
		return velocidadY;
	}

	public void setVelocidadY(double velocidadY) {
		this.velocidadY = velocidadY;
	}

	public double getGravity() {
		return gravity;
	}

	public void setGravity(double gravity) {
		this.gravity = gravity;
	}

	public boolean isOnGround() {
		return onGround;
	}

	public void setOnGround(boolean onGround) {
		this.onGround = onGround;
	}

	public boolean isSeAgacho() {
		return seAgacho;
	}

	public void setSeAgacho(boolean seAgacho) {
		this.seAgacho = seAgacho;
	}

	public int getVidas() {
		return vidas;
	}

	public void setVidas(int vidas) {
		this.vidas = vidas;
	}

	public Image getImagenParado() {
		return imagenParado;
	}

	public void setImagenParado(Image imagenParado) {
		this.imagenParado = imagenParado;
	}

	public int getDireccion() {
		return direccion;
	}

	public void setDireccion(int direccion) {
		this.direccion = direccion;
	}

	public Laser disparar() {		
		return new Laser(x,y,direccion);
	}
	
	
	
	
	//hasta aca terminan los getters y setters
}
