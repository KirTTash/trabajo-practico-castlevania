package juego;

import java.awt.Color;

import entorno.*;

public class Piso { 
	private double PosX;
	private double PosY;
	private double Largo;
	private double Alto;
		
	public Piso(double PosX, double PosY, double Alto, double Largo) { // Constructor de Piso
		this.PosX = PosX;
		this.PosY= PosY;
		this.Alto = Alto;
		this.Largo = Largo;
	}
	
	public void DibujarPiso(Entorno entorno) { // Dibujador de Piso
		entorno.dibujarRectangulo(this.PosX, this.PosY, this.Largo, this.Alto, 0, Color.magenta);
		
	}
	
	
	//////////////////////////////////////////////////GETTERS AND SETTERS ///////////////////////////////////////////////////////

	public double getPosX() {
		return PosX;
	}

	public void setPosX(int posX) {
		PosX = posX;
	}

	public double getPosY() {
		return PosY;
	}

	public void setPosY(int posY) {
		PosY = posY;
	}

	public double getAlto() {
		return Alto;
	}

	public void setAlto(int alto) {
		Alto = alto;
	}

	public double getLargo() {
		return Largo;
	}

	public void setLargo(int largo) {
		Largo = largo;
	}
	
	
	
	
	
}
